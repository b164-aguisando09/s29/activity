const express = require('express');

const app = express();

const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended:true }));


let users = []


app.get("/home", (req, res) => {
	res.send("Welcome to the homepage!")
});

app.get("/users", (req, res) => {
	res.send(users);
});



app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`)
	} else{
		res.send("Please input BOTH username and password")
	}

	console.log(users);
});

app.delete("/delete-user", (req, res) => {
	console.log(req.body);
	let message;

	if (users.length != 0) {

		for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){

			users.splice(users[i], 1);

		 
			message = `User ${req.body.username}'s account has been deleted`;

			break;

		} else {
			message = "User and password does not exist or does not match. Please insert valid account."
			break;
		}
	}
	} else	{
		message = "There is no record";
	}

	res.send(message);

});


app.listen(port, ()=> console.log(`Server running at port:${port}`));